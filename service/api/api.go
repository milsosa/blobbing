package api

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/milsosa/blobbing/model"
)

type httpAPI struct {
	repository model.Repository
}

type response struct {
	Items interface{} `json:"items"`
	Error *string     `json:"error"`
}

func Setup(router *mux.Router, repository model.Repository) {
	api := httpAPI{repository: repository}
	api.registerRoutes(router)
}

func (api *httpAPI) registerRoutes(router *mux.Router) {
	router.NewRoute().
		Methods("GET").
		Path("/blobs").
		Name("ListBlobs").
		HandlerFunc(api.listBlobs)
}

func (api *httpAPI) listBlobs(rw http.ResponseWriter, r *http.Request) {
	blobs, err := api.repository.List(r.Context())
	if err != nil {
		log.Printf("failed to list blobs: %s", err)

		rw.WriteHeader(http.StatusInternalServerError)
		respErr := "Unexpected error listing blobs"
		res := response{Error: &respErr, Items: blobs}

		if err := json.NewEncoder(rw).Encode(res); err != nil {
			log.Printf("failed writing response: %s", err)
		}
		return
	}

	if err := json.NewEncoder(rw).Encode(response{Items: blobs}); err != nil {
		log.Printf("failed writing response: %s", err)
	}
}
