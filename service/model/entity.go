package model

import "time"

type Blob struct {
	Name         string     `json:"name"`
	LastModified *time.Time `json:"last_modified"`
	Size         int64      `json:"size"`
}
