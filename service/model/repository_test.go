package model

import (
	"context"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/milsosa/blobbing/client"
	"gitlab.com/milsosa/blobbing/mocks"
)

//go:generate mockgen -package=mocks -destination ../mocks/client.go gitlab.com/milsosa/blobbing/client Client

func TestListBlobs(t *testing.T) {
	ctrl := gomock.NewController(t)
	clientMock := mocks.NewMockClient(ctrl)

	ctx := context.Background()
	items := []client.Item{
		{Name: "blob-1", LastModified: timePtr(time.Now()), Size: 10},
		{Name: "blob-2", LastModified: timePtr(time.Now()), Size: 20},
		{Name: "blob-3", LastModified: timePtr(time.Now()), Size: 30},
	}

	clientMock.EXPECT().List(ctx).Return(items, nil)

	repository := NewRepository(clientMock)
	blobs, err := repository.List(ctx)
	assert.NoError(t, err)
	assert.Equal(t, len(items), len(blobs))
}

func timePtr(t time.Time) *time.Time {
	return &t
}
