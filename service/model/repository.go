package model

import (
	"context"
	"fmt"

	"gitlab.com/milsosa/blobbing/client"
)

type Repository interface {
	List(context.Context) ([]Blob, error)
}

type repository struct {
	client client.Client
}

func NewRepository(storeClient client.Client) *repository {
	return &repository{client: storeClient}
}

func (r *repository) List(ctx context.Context) ([]Blob, error) {
	items, err := r.client.List(ctx)
	if err != nil {
		return []Blob{}, fmt.Errorf("failed to load blobs: %w", err)
	}
	blobs := make([]Blob, 0, len(items))
	for _, item := range items {
		blobs = append(blobs, Blob{
			Name:         item.Name,
			LastModified: item.LastModified,
			Size:         item.Size,
		})
	}
	return blobs, nil
}
