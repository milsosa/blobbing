# blobbing service

Implements a HTTP API and a command to allow listing blobs from the provided S3 bucket.

## Requirements

It is expected that the environment where the resulting Docker image or the command is run, to be configured with the appropriate AWS credentials.
- `AWS_ACCESS_KEY_ID`
- `AWS_SECRET_ACCESS_KEY`
- `AWS_REGION`

Such credentials should have permissions to list objects from the provided S3 bucket.

Specify the S3 bucket to be used to list its blobs through the environment variable: `BLOBS_STORAGE_BUCKET`. This should be the output from the `infra` component.

## How tu run

Enter into the `service` directory and then, either:

- Run the `BLOBS_STORAGE_BUCKET={bucket-name} go run cmd/list.go`

OR

1. Run `make build-image` to build the Docker image.
2. Run `make run-api` to start the HTTP API or `make list-blobs` to directly list the blobs.
