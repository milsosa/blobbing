package helpers

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"gitlab.com/milsosa/blobbing/client"
	"gitlab.com/milsosa/blobbing/model"
)

const (
	blobsStorageBucket = "BLOBS_STORAGE_BUCKET"
)

func BuildRepository() (model.Repository, error) {
	cfg, err := config.LoadDefaultConfig(context.TODO())
	if err != nil {
		return nil, fmt.Errorf("unable to load aws config, %w", err)
	}

	s3ApiClient := s3.NewFromConfig(cfg)
	bucketName := getStringEnvOrDie(blobsStorageBucket)

	log.Printf("configuring client with bucket: %s", bucketName)
	blobsClient := client.NewClient(s3ApiClient, bucketName)

	return model.NewRepository(blobsClient), nil
}

func getStringEnvOrDie(envName string) string {
	value := os.Getenv(envName)
	if value == "" {
		log.Fatalf("required environment %s was not provided", envName)
	}
	return value
}
