package client

import (
	"context"
	"time"

	"github.com/aws/aws-sdk-go-v2/service/s3"
)

type Client interface {
	List(context.Context) ([]Item, error)
}

type s3Client struct {
	apiClient *s3.Client
	bucket    string
}

type Item struct {
	Name         string
	LastModified *time.Time
	Size         int64
}

func NewClient(apiClient *s3.Client, bucket string) *s3Client {
	return &s3Client{apiClient: apiClient, bucket: bucket}
}

func (c *s3Client) List(ctx context.Context) ([]Item, error) {
	res, err := c.apiClient.ListObjects(ctx, &s3.ListObjectsInput{
		Bucket: &c.bucket,
	})
	if err != nil {
		return nil, err
	}

	items := make([]Item, 0, len(res.Contents))
	for _, o := range res.Contents {
		lastModified := o.LastModified.UTC()
		items = append(items, Item{
			Name:         *o.Key,
			LastModified: &lastModified,
			Size:         o.Size,
		})
	}
	return items, nil
}
