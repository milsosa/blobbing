package main

import (
	"context"
	"encoding/json"
	"log"

	"gitlab.com/milsosa/blobbing/helpers"
)

func main() {
	repository, err := helpers.BuildRepository()
	if err != nil {
		log.Fatalf("failed to initialize repository: %s", err)
	}
	blobs, err := repository.List(context.Background())

	if err != nil {
		log.Fatalf("failed to list blobs: %s", err)
	}

	log.Printf("Found %d blobs:\n", len(blobs))
	blobsJson, err := json.MarshalIndent(blobs, "", "  ")
	if err != nil {
		log.Fatalf("failed marshal blobs: %s", err)
	}
	log.Printf("%s", blobsJson)
}
