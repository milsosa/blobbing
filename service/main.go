package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/milsosa/blobbing/api"
	"gitlab.com/milsosa/blobbing/helpers"
)

func main() {
	blobsRepository, err := helpers.BuildRepository()
	if err != nil {
		log.Fatalf("failed to initialize repository: %s", err)
	}

	router := mux.NewRouter().StrictSlash(true)
	api.Setup(router, blobsRepository)

	log.Printf("running server on port 8080")
	log.Fatal(http.ListenAndServe(":8080", router))
}
