# blobbing

Blobbing provides the functionality of listing blobs from a given AWS S3 bucket.

It is composed of two components, the `infra` and `service`.

The `infra` component creates the corresponding infrastructure that the `service` requires to run.

The `service`, once built, will provide a Docker image that when run, will expose a
HTTP API on the port `8080` (static for now inside the Docker image).

Additionally, the Docker image has a basic command (CLI) built on the `/opt/app/list-blobs`

## Running

Look at the readme of the `infra` and `service` components for details.

## Possible CI/CD implementation

For the infra, there could be a pipeline defined with the `plan` and `deploy` jobs. The deploy could be a manual and restricted job.

For the service, there would be `test`, `build` and `deploy` jobs. The `deploy` would push the image to a configured Docker registry.

The credentials could be set using Gitlab's CI environments and variables settings.

## Result examples

With HTTP API (`make run-api`):
```
❯ curl -s http://localhost:8080/blobs | jq
{
  "items": [
    {
      "name": "blob-1.svg",
      "last_modified": "2021-10-10T11:02:25Z",
      "size": 558
    },
    {
      "name": "blobbing.jpeg",
      "last_modified": "2021-10-10T11:02:24Z",
      "size": 108131
    },
    {
      "name": "dora_research_program.pdf",
      "last_modified": "2021-10-10T11:02:24Z",
      "size": 56035
    }
  ],
  "error": null
}
```

With `make list-blobs` command:
```
❯ make list-blobs
2021/10/10 11:03:41 configuring client with bucket: uniq-prefix-blobs-store-dev
2021/10/10 11:03:42 Found 3 blobs:
2021/10/10 11:03:42 [
  {
    "name": "blob-1.svg",
    "last_modified": "2021-10-10T11:02:25Z",
    "size": 558
  },
  {
    "name": "blobbing.jpeg",
    "last_modified": "2021-10-10T11:02:24Z",
    "size": 108131
  },
  {
    "name": "dora_research_program.pdf",
    "last_modified": "2021-10-10T11:02:24Z",
    "size": 56035
  }
]
```