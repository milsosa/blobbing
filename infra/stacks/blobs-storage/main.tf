
module "blobs_bucket" {
  source = "../../modules/bucket"
  bucket_name = local.uniq_blobs_bucket_name
}