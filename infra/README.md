# blobbing infra

This component defines the required architecture by the `service` component through the usage of Terraform.

## Folder structure

- `config` is meant to have `tfvars` files defined per environment, for now only `dev.tfvars` is defined.
- `modules` is meant to contain the reusable modules, for now only a simple `bucket` module is defined.
- `stacks` is meant to contain the different stacks, for now there is the `blobs-storage` one only.
  - `blobs-storage` only creates a S3 bucket for storing blobs by using the `config/dev.tfvars` and the `modules/bucket` module.

## Requirements

It is expected that the environment where the commands to create the infra will be run is already configured with appropriate AWS credentials envs.
- `AWS_ACCESS_KEY_ID`
- `AWS_SECRET_ACCESS_KEY`
- `AWS_REGION`

Such credentials should have permissions to create/update/delete resources on AWS.

## How to run

Enter into the `stacks/blobs-storage` folder and execute the following commands:

1. `make init` - To initialize terraform provider and modules
2. `make plan` - To define the resources to be created/updated
3. `make apply` - To apply the planned resources actions with the previous command

## Notes

As a best practice, there should be a user role defined with restricted permissions to only do the allowed operations on the created bucket.

At the time of running the service or command, this role should be assumed.

On Kubernetes, this would be done through using the defined role as service account.
