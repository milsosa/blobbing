resource "aws_s3_bucket" "bucket" {
  bucket = var.bucket_name
  acl = "private"

  lifecycle_rule {
    enabled = true
    expiration {
      days = 30
    }
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "aws:kms"
      }
      bucket_key_enabled = true
    }
  }
}
